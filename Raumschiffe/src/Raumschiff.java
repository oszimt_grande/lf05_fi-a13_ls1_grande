import java.util.ArrayList;
/**
 * Fachklasse zum Abbilden der Raumschiffe
 * @author grande
 */
public class Raumschiff {
	//Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstruktoren
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	//Verwaltungsmethoden
	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ermitteln und R�ckgabe der Anzahl der geladenen Torpedos
	 * @return photonentorpedoAnzahl Die Anzahl der Torpedos
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen der Torpedoanzahl
	 * @param photonentorpedoAnzahl Die Anzahl der Torpedos
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ermitteln und R�ckgabe des Energieversorgungsstands in Prozent
	 * @return energieversorgungInProzent Der Energieversorgungsstand in Prozent
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen des Energieversorgungsstands in Prozent
	 * @param energieversorgungInProzent Der Energieversorgungsstand in Prozent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * Vorausetzung: keine
	 * (Effekt:)Ermitteln und R�ckgabe des Schildstands in Prozent
	 * @return schildeInProzent Der Schildstand in Prozent
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen des Schildstands in Prozent
	 * @param schildeInProzent Der Schildstand in Prozent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	/**
	 * Voraussetzung: keine
	 * Ermitteln und R�ckgabe des H�llenstands in Prozent
	 * @return huelleInProzent
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen des H�llenstands in Prozent
	 * @param huelleInProzent Der H�llenstand in Prozent
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ermitteln und R�ckgabe des Stands der Lebenserhaltungssysteme in Prozent
	 * @return lebenserhaltungssystemeInProzent Der Stand der Lebenserhaltungssystem in Prozent
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen des Stands der Lebenserhaltungssysteme in Prozent
	 * @param lebenserhaltungssystemeInProzent Der Stand der Lebenserhaltungssysteme in Prozent
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ermitteln und R�ckgabe der Androidenanzahl
	 * @return androidenAnzahl Die Anzahl der Androiden
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen der Androidenanzahl
	 * @param androidenAnzahl Die Anzahl der Androiden
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ermitteln und R�ckgabe des Schiffsnamens
	 * @return schiffsname Der Name des Schiffs
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen des Schiffsnamens
	 * @param schiffsname Der Name des Schiffs
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	//Allgemeine Methoden
	/**
	 * Ladungsobjekt zum Ladungsverzeichnis hinzuf�gen
	 * Voraussetzung: �bergabe eines Ladungsobjekts
	 * (Effekt:)Neues Ladungsobjekt wird dem Raumschiffobjekt unterstellt
	 * @param neueLadung
	 */
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ausgeben aller Attribute des aktuellen Raumschiffobjekts auf die Konsole
	 */
	public void zustandAusgeben() {
		System.out.printf("Zustand Raumschiff %s\n\n", this.schiffsname);
		System.out.printf("Anzahl Photonentorpedos: %d\n", this.photonentorpedoAnzahl);
		System.out.printf("Energieversorgung: %d%%\n", this.energieversorgungInProzent);
		System.out.printf("Schilde: %d%%\n", this.schildeInProzent);
		System.out.printf("H�lle: %d%%\n", this.huelleInProzent);
		System.out.printf("Lebenserhaltung: %d%%\n", this.lebenserhaltungssystemeInProzent);
		System.out.printf("Anzahl Reparaturandroiden: %d\n", this.androidenAnzahl);
	}
	
	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ausgeben des Ladungsverzeichnisses des aktuellen Raumschiffobjekts auf die Konsole
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.print("Ladungsverzeichnis\n");
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.printf("Ladung: %s\nMenge: %d\n\n",
					ladungsverzeichnis.get(i).getBezeichnung(),
					ladungsverzeichnis.get(i).getMenge());
		}
	}
	
	/**
	 * Abschie�en eines geladenen Torpedos auf ein Raumschiffobjekt
	 * Voraussetzung: keine
	 * (Effekt:)Ausgeben auf der Konsole, Vermerken eines Treffers beim Ziel und Fehlermeldung, falls keine Torpedos geladen sind.
	 * @param ziel
	 */
	public void ptorpedosAbschie�en(Raumschiff ziel) {
		if(this.photonentorpedoAnzahl < 1) {
			System.out.println("-=*Click*=-");
		}
		else {
			this.photonentorpedoAnzahl--;
			ziel.trefferVermerken();
			this.broadcastKommunikator.add("Photonentorpedo abgeschossen");
			this.broadcastKommunikator.add((ziel.getSchiffsname() + "wurde getroffen!"));
		}
	}
	
	/**
	 * Schie�en mit dem Phaser auf ein Raumschiffobjekt
	 * Voraussetzung: keine
	 * (Effekt:)inkl. Ausgeben auf der Konsole, Vermerken eines Treffers beim Ziel, Fehlermeldung falls das eigene Energieniveau
	 * nicht ausreicht und Reduzierung des eigenen Energielevels bei erfolgreichem Schuss
	 * @param ziel
	 */
	public void phaserAbschie�en(Raumschiff ziel) {
		if(this.energieversorgungInProzent < 50) {
			System.out.println("-=*Click*=-");
		}
		else {
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			ziel.trefferVermerken();
			this.broadcastKommunikator.add("Phaserkanone abgeschossen");
		}
	}
	
	/**
	 * Voraussetzung: Anderes Raumschiffobjekt muss seine Methode "phaserSchie�en" oder "ptorpedoSchie�en" aufrufen
	 * (Effekt:) Meldung auf der Konsole und Reduzierung des Schildlevels
	 * Sollte das Schildlevel bei 0% oder weniger liegen, wird die H�llenst�rke
	 * reduziert. Sollte diese ebenfalls 0% oder weniger betragen, wird das Level der
	 * Lebenserhaltungssysteme auf 0 gesetzt und dies auf der Konsole vermerkt.
	 */
	public void trefferVermerken() {
		System.out.print(this.schiffsname + " wurde getroffen!\n");
		this.schildeInProzent = this.schildeInProzent - 50;
		if(this.schildeInProzent <= 0) {
			this.huelleInProzent = this.huelleInProzent - 50;
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			
			if(this.huelleInProzent <= 0) {
				this.lebenserhaltungssystemeInProzent = 0;
				nachrichtenAnAlle("Die Lebenserhaltungssysteme wurden vernichtet!\n");
			}
		}
	}
	
	/**
	 * (Effekt:)Eine Nachricht (String) wird dem eigenen BroadcastKommunikator hinzugef�gt.
	 * @param nachricht
	 */
	public void nachrichtenAnAlle(String nachricht) {
		this.broadcastKommunikator.add(nachricht);
	}
	
	/**
	 * Voraussetzung: keine
	 * (Effekt:)Der gesamte Inhalt des BroadcastKommunikators wird auf der Konsole ausgegeben.
	 */
	public void logbuchZurueckgeben() {
		System.out.println("Logbuch der " + this.schiffsname);
		for(int i = 0; i < broadcastKommunikator.size(); i++) {
			System.out.printf("Nachricht %d: " + broadcastKommunikator.get(i) + "\n", i);
		}
		System.out.print("\n");
	}
}
