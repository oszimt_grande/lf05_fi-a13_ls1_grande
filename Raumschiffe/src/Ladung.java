/**
 * Fachklasse zum Abbilden der Ladung
 * @author Christoph Grande
 */

public class Ladung {
	//Attribute
	private String bezeichnung;
	private int menge;
	
	//Konstruktor(en)
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	//Verwaltungsmethoden
	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ermitteln und R�ckgabe der Ladungsbezeichnung
	 * @return bezeichnung
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen der Ladungsbezeichnung
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Ermitteln und R�ckgabe der Menge der Ladung
	 * @return menge
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * Voraussetzung: keine
	 * (Effekt:)Setzen der Menge der Ladung
	 * @param menge
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	//allgemeine Methoden
	/**
	 * Voraussetzung: keine
	 * (Effekt:)Die ToString-Methode wird �berschrieben und durch eine Ausgabe des Objektinhalts
	 * als String in angepasstem Format ersetzt.
	 */
	@Override
	public String toString() {
		String returnString = "Ladung, " + this.bezeichnung + ", " + this.menge;
		return returnString;
	}
}
