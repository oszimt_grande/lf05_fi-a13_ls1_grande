/**
 * Testklasse zu "Raumschiffe" und "Ladung"
 * @author Christoph Grande
 */

public class TestRaumschiffe {
	/**
	 * Main-Methode zum Testen, die die Klassen "Raumschiff" und "Ladung" entsprechend der
		Aufgabenstellung verwendet
	 */
	public static void main(String[] args) {
		//Objekterzeugung
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 1, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung batleth = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung borgschrott = new Ladung("Borg-Schrott", 5);
		Ladung rotematerie = new Ladung("Rote Materie", 2);
		Ladung plasmawaffe = new Ladung("Plasma-Waffe", 50);
		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Ladung ptopedo = new Ladung("Photonentorpedo", 3);
		
		//Objektmanipulation
		klingonen.addLadung(schneckensaft);
		klingonen.addLadung(batleth);
		romulaner.addLadung(borgschrott);
		romulaner.addLadung(rotematerie);
		romulaner.addLadung(plasmawaffe);
		vulkanier.addLadung(forschungssonde);
		vulkanier.addLadung(ptopedo);
		
		//weitere Methodenaufrufe
		//Ereignisablauf:
		klingonen.ptorpedosAbschießen(romulaner);
		romulaner.phaserAbschießen(klingonen);
		vulkanier.nachrichtenAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandAusgeben();
		klingonen.ladungsverzeichnisAusgeben();
		klingonen.ptorpedosAbschießen(romulaner);
		klingonen.ptorpedosAbschießen(romulaner);
		klingonen.zustandAusgeben();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandAusgeben();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandAusgeben();
		vulkanier.ladungsverzeichnisAusgeben();
		klingonen.logbuchZurueckgeben();
		romulaner.logbuchZurueckgeben();
		vulkanier.logbuchZurueckgeben();
	}
}
