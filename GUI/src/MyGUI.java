import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtInput;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGUI frame = new MyGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MyGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 565, 815);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblOutput = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblOutput.setBounds(15, 16, 513, 50);
		contentPane.add(lblOutput);
		
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(15, 82, 262, 20);
		contentPane.add(lblAufgabe1);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				contentPane.setBackground(Color.RED);
			}
		});
		btnRot.setBounds(15, 118, 140, 29);
		contentPane.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(198, 118, 140, 29);
		contentPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(388, 118, 140, 29);
		contentPane.add(btnBlau);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(15, 218, 262, 20);
		contentPane.add(lblAufgabe2);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(15, 163, 140, 29);
		contentPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(new Color (0xEEEEEE));
			}
		});
		btnStandardfarbe.setBounds(198, 163, 140, 29);
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//wip
				//contentPane.setBackground();
			}
		});
		btnFarbeWhlen.setBounds(388, 163, 140, 29);
		contentPane.add(btnFarbeWhlen);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int fsize = lblOutput.getFont().getSize();
				lblOutput.setFont(new Font ("Arial", Font.PLAIN, fsize));
			}
		});
		btnArial.setBounds(15, 254, 140, 29);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int fsize = lblOutput.getFont().getSize();
				lblOutput.setFont(new Font ("Comic Sans MS", Font.PLAIN, fsize));
			}
		});
		btnComicSansMs.setBounds(198, 254, 140, 29);
		contentPane.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int fsize = lblOutput.getFont().getSize();
				lblOutput.setFont(new Font ("Courier New", Font.PLAIN, fsize));
			}
		});
		btnCourierNew.setBounds(388, 254, 140, 29);
		contentPane.add(btnCourierNew);
		
		txtInput = new JTextField();
		txtInput.setText("Hier bitte Text eingeben");
		txtInput.setBounds(15, 309, 513, 26);
		contentPane.add(txtInput);
		txtInput.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOutput.setText(txtInput.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(15, 351, 235, 29);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOutput.setText("");
			}
		});
		btnTextImLabel.setBounds(293, 351, 235, 29);
		contentPane.add(btnTextImLabel);
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(15, 396, 262, 20);
		contentPane.add(lblAufgabe3);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOutput.setForeground(Color.red);
			}
		});
		btnRot_1.setBounds(15, 432, 140, 29);
		contentPane.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOutput.setForeground(Color.blue);
			}
		});
		btnBlau_1.setBounds(198, 432, 140, 29);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOutput.setForeground(Color.black);
			}
		});
		btnSchwarz.setBounds(388, 432, 140, 29);
		contentPane.add(btnSchwarz);
		
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabe4.setBounds(15, 477, 262, 20);
		contentPane.add(lblAufgabe4);
		
		JButton buttonPlus = new JButton("+");
		buttonPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblOutput.getFont().getSize();
				String ftype = lblOutput.getFont().getFontName();
				lblOutput.setFont(new Font(ftype, Font.PLAIN, groesse + 1));
			}
		});
		buttonPlus.setBounds(15, 513, 235, 29);
		contentPane.add(buttonPlus);
		
		JButton buttonMinus = new JButton("-");
		buttonMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblOutput.getFont().getSize();
				if (groesse > 0) {
					String ftype = lblOutput.getFont().getFontName();
					lblOutput.setFont(new Font(ftype, Font.PLAIN, groesse - 1));
				}
			}
		});
		buttonMinus.setBounds(293, 513, 235, 29);
		contentPane.add(buttonMinus);
		
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(15, 558, 262, 20);
		contentPane.add(lblAufgabe5);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOutput.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setBounds(15, 594, 140, 29);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOutput.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(198, 594, 140, 29);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOutput.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(388, 594, 140, 29);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setBounds(15, 639, 262, 20);
		contentPane.add(lblAufgabe6);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(15, 675, 513, 84);
		contentPane.add(btnExit);
	}
}
