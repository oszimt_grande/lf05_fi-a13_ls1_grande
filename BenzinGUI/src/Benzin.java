public class Benzin {
	public static double BerechnungVerbrauch(double vliter, double gkm) {
		if (gkm == 100.0) {
			return vliter;
		}
		else {
			double faktor = 100.0 / gkm;
			return vliter * faktor;
		}
	}
	
	public static double BerechnungPreis(double verbrauch, double lpreis) {
		return verbrauch * lpreis;
	}
	
	public static double BerechnungPreisGesamt(double preis, double vliter) {
		return preis * vliter;
	}
}
