import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BenzinGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtLiterpreis;
	private JTextField txtVerbrauchteLiter;
	private JTextField txtgefahreneKm;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BenzinGUI frame = new BenzinGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BenzinGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 571, 629);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitle = new JLabel("Benzinkalkulator");
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblTitle.setBounds(165, 16, 186, 36);
		contentPane.add(lblTitle);
		
		JLabel lblLiterpreis = new JLabel("Literpreis:");
		lblLiterpreis.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLiterpreis.setBounds(114, 74, 77, 23);
		contentPane.add(lblLiterpreis);
		
		JLabel lblVerbrauchteLiter = new JLabel("Verbrauchte Liter:");
		lblVerbrauchteLiter.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVerbrauchteLiter.setBounds(51, 113, 140, 23);
		contentPane.add(lblVerbrauchteLiter);
		
		JLabel lblGefahreneKm = new JLabel("gefahrene km:");
		lblGefahreneKm.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblGefahreneKm.setBounds(76, 152, 114, 24);
		contentPane.add(lblGefahreneKm);
		
		txtLiterpreis = new JTextField();
		txtLiterpreis.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtLiterpreis.setBounds(206, 72, 146, 26);
		contentPane.add(txtLiterpreis);
		txtLiterpreis.setColumns(10);
		
		txtVerbrauchteLiter = new JTextField();
		txtVerbrauchteLiter.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtVerbrauchteLiter.setBounds(205, 110, 146, 26);
		contentPane.add(txtVerbrauchteLiter);
		txtVerbrauchteLiter.setColumns(10);
		
		txtgefahreneKm = new JTextField();
		txtgefahreneKm.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtgefahreneKm.setBounds(205, 151, 146, 26);
		contentPane.add(txtgefahreneKm);
		txtgefahreneKm.setColumns(10);
		
		JLabel lblVerbrauchProkm = new JLabel("Verbrauch pro 100km:");
		lblVerbrauchProkm.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVerbrauchProkm.setBounds(15, 284, 186, 23);
		contentPane.add(lblVerbrauchProkm);
		
		JLabel lblVpkm = new JLabel("");
		lblVpkm.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVpkm.setBounds(206, 286, 140, 20);
		contentPane.add(lblVpkm);
		
		JLabel lblPreisProkm = new JLabel("Preis pro 100km:");
		lblPreisProkm.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPreisProkm.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPreisProkm.setBounds(15, 323, 176, 20);
		contentPane.add(lblPreisProkm);
		
		JLabel lblGesamtpreis = new JLabel("Gesamtpreis:");
		lblGesamtpreis.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGesamtpreis.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblGesamtpreis.setBounds(15, 359, 176, 20);
		contentPane.add(lblGesamtpreis);
		
		JLabel lblPpkm = new JLabel("");
		lblPpkm.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPpkm.setBounds(206, 323, 145, 20);
		contentPane.add(lblPpkm);
		
		JLabel lblGPreis = new JLabel("");
		lblGPreis.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblGPreis.setBounds(206, 359, 140, 20);
		contentPane.add(lblGPreis);
		
		JButton btnBerechne = new JButton("Berechne");
		btnBerechne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Deklaration
				double lpreis = Double.parseDouble(txtLiterpreis.getText());
				double vliter = Double.parseDouble(txtVerbrauchteLiter.getText());
				double gkm = Double.parseDouble(txtgefahreneKm.getText());
				
				//Berechnung
				double verbrauch = Benzin.BerechnungVerbrauch(vliter, gkm);
				String verbrauchString = String.valueOf(verbrauch);
				double preis = Benzin.BerechnungPreis(verbrauch, lpreis);
				String preisString = String.valueOf(preis);
				double gpreis = Benzin.BerechnungPreisGesamt(preis, vliter);
				String gpreisString = String.valueOf(gpreis);
				
				//Ausgabe
				lblVpkm.setText(verbrauchString);
				lblPpkm.setText(preisString);
				lblGPreis.setText(gpreisString);
				//Farbgebung
				if (verbrauch < 5) {
					lblVpkm.setForeground(Color.GREEN);
				}
				else if (verbrauch <= 10) {
					lblVpkm.setForeground(Color.YELLOW);
				}
				else if (verbrauch > 10) {
					lblVpkm.setForeground(Color.RED);
				}
			}
		});
		btnBerechne.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnBerechne.setBounds(206, 212, 145, 58);
		contentPane.add(btnBerechne);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblVpkm.setText("");
				lblPpkm.setText("");
				lblGPreis.setText("");
				txtLiterpreis.setText("");
				txtVerbrauchteLiter.setText("");
				txtgefahreneKm.setText("");
			}
		});
		btnClear.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnClear.setBounds(46, 212, 145, 58);
		contentPane.add(btnClear);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnExit.setBounds(46, 431, 456, 64);
		contentPane.add(btnExit);
	}
}
