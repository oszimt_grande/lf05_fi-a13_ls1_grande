
public class Aufgabe5 {

	public static void main(String[] args) {
		
		//Zeile 1
		System.out.printf("%-5s", "0!");
		System.out.printf("%s", "=");
		System.out.printf("%19s", "");
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", 1);
		
		//Zeile 2
		System.out.printf("%-5s", "1!");
		System.out.printf("%s", "=");
		System.out.printf(" %-18s", "1");
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", 1);
		
		//Zeile 3
		System.out.printf("%-5s", "2!");
		System.out.printf("%s", "=");
		System.out.printf(" %-18s", "1 * 2");
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", 2);
		
		//Zeile 3
		System.out.printf("%-5s", "3!");
		System.out.printf("%s", "=");
		System.out.printf(" %-18s", "1 * 2 * 3");
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", 6);
		
		//Zeile 4
		System.out.printf("%-5s", "4!");
		System.out.printf("%s", "=");
		System.out.printf(" %-18s", "1 * 2 * 3 * 4");
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", 24);
		
		//Zeile 5
		System.out.printf("%-5s", "5!");
		System.out.printf("%s", "=");
		System.out.printf(" %-18s", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", 120);
	}

}
