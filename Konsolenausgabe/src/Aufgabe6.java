
public class Aufgabe6 {

	public static void main(String[] args) {
		System.out.printf("%-12s", "Fahrenheit");
		System.out.printf("%s", "|");
		System.out.printf("%10s", "Celsius\n");
		
		System.out.println("-----------------------");
		
		//Zeile 1
		System.out.printf("%-12d", -20);
		System.out.printf("%s", "|");
		System.out.printf("%10.2f\n", -28.8889);
		
		//Zeile 2
		System.out.printf("%-12d", -10);
		System.out.printf("%s", "|");
		System.out.printf("%10.2f\n", -23.3333);
		
		//Zeile 3
		System.out.printf("+%-11d", 0);
		System.out.printf("%s", "|");
		System.out.printf("%10.2f\n", -17.7778);
		
		//Zeile 4
		System.out.printf("+%-11d", 20);
		System.out.printf("%s", "|");
		System.out.printf("%10.2f\n", -6.6667);
		
		//Zeile 5
		System.out.printf("+%-11d", 30);
		System.out.printf("%s", "|");
		System.out.printf("%10.2f\n", -1.1111);

	}

}
