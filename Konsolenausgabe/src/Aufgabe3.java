
public class Aufgabe3 {

	public static void main(String[] args) {
		double[] numbers = new double[5];
		numbers[0] = 22.4234234;
		numbers[1] = 111.2222;
		numbers[2] = 4.0;
		numbers[3] = 1000000.551;
		numbers[4] = 97.34;
		
		System.out.printf("%.2f\n", numbers[0]);
		System.out.printf("%.2f\n", numbers[1]);
		System.out.printf("%.2f\n", numbers[2]);
		System.out.printf("%.2f\n", numbers[3]);
		System.out.printf("%.2f\n", numbers[4]);
	}

}
