public class ArrayHelper {

	public static void main(String[] args) {
		int[] zahlenArray = {1, 2, 3, 4, 5, 6, 7, 8, 9,};
		String zeichenString = convertArrayToString(zahlenArray);
		
		//Test
		System.out.println(zeichenString);
	}
	
	public static String convertArrayToString(int[] zahlen) {
		String zeichen = "";
		
		for (int i = 0; i < zahlen.length; i++) {
			zeichen = zeichen + zahlen[i];
		}
		
		return zeichen;
	}
}
