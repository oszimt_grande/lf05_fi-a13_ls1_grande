public class GanzzahlenDrehen2 {

	public static void main(String[] args) {
		int[] ganzzahlen = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		int[] gedrehteZahlen = new int[ganzzahlen.length];
		gedrehteZahlen = drehen(ganzzahlen);
		
		//Test
		for (int i = 0; i < gedrehteZahlen.length; i++) {
			System.out.print(gedrehteZahlen[i]);
		}
	}
	
	public static int[] drehen(int[] zahlen) {
		int zahlVorn, zahlHinten, z;
		int[] getauschteZahlen = new int[zahlen.length];
		
		for (int i = 0; i < (zahlen.length/2); i++) {
			z = (zahlen.length-1)-i;
			
			zahlVorn = zahlen[i];
			zahlHinten = zahlen[z];
			
			getauschteZahlen[i] = zahlHinten;
			getauschteZahlen[z] = zahlVorn;
		}
		
		return getauschteZahlen;
	}
}
