public class FahrenheitUmrechnen {

	public static void main(String[] args) {
		double[] temps = {10.0, 20.0, 30.0, 40.0, 50.0};
		int laenge = temps.length;
		
		double[][] temps2 = umrechnung(laenge, temps);
		
		//Test
		for (int i = 0; i < temps2.length; i++) {
			System.out.print(temps2[i][0] + "   " + temps2[i][1] + "\n");
		}
		
	}
	
	public static double[][] umrechnung(int laenge, double[] temps) {
		double[][] tempTable = new double[laenge][2];
		
		//Fahrenheit-Werte in 2D-Array schreiben
		for (int i = 0; i < laenge; i++) {
			tempTable[i][0] = temps[i];
		}
		
		//Celsius-Werte berechnen
		for (int i = 0; i < laenge; i++) {
			tempTable[i][1] = (5.0/9.0) * (tempTable[i][0] -32);
		}
		
		return tempTable;
	}

}
