
public class GanzzahlenDrehen {

	public static void main(String[] args) {
		int[] ganzzahlen = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		drehen(ganzzahlen);
		
		//Test
		for (int i = 0; i < ganzzahlen.length; i++) {
			System.out.print(ganzzahlen[i]);
		}
	}
	
	public static void drehen(int[] zahlen) {
		int zahlVorn, zahlHinten, z;
		
		for (int i = 0; i < (zahlen.length/2); i++) {
			z = (zahlen.length-1)-i;
			
			zahlVorn = zahlen[i];
			zahlHinten = zahlen[z];
			
			zahlen[i] = zahlHinten;
			zahlen[z] = zahlVorn;
		}
	}

}
