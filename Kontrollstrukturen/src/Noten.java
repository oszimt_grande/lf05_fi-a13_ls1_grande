import java.util.Scanner;

public class Noten {

	static Scanner tastatur = new Scanner(System.in); 
	
	public static void main(String[] args) {
		short note;
		
		//Eingabe
		System.out.print("Note eingeben: ");
		note = tastatur.nextShort();
		
		switch (note)
		{
			case 1:
				System.out.println("Sehr gut");
				break;
			case 2:
				System.out.println("Gut");
				break;
			case 3:
				System.out.println("Befriedigend");
				break;
			case 4:
				System.out.println("Ausreichend");
				break;
			case 5:
				System.out.println("Mangelhaft");
				break;
			case 6:
				System.out.println("Ungenügend");
				break;
			default:
				System.out.println("Fehler! Bitte eine Ganzzahl zwischen 1 und 6 eingeben!");
		}
	}

}
