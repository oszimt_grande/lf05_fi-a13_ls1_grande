import java.util.Scanner;

public class Fakultaet {

	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		int n;
		int index;
		int fakultaet;
		
		do {
			System.out.print("Natürliche Zahl bis 20 eingeben: ");
			n = tastatur.nextInt();
		}
		while(n > 20 || n < 0);
		
		index = 1;
		fakultaet = 1;
		
		while(index <= n) {
			fakultaet = fakultaet * index;
			
			index++;
		}
		
		System.out.printf("%d! = %d", n, fakultaet);
	}

}
