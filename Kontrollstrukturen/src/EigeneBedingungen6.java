import java.util.Scanner;

class EigeneBedingungen6 {
	
	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		double zahl1;
		double zahl2;
		double zahl3;
		
		System.out.print("Zahl 1 eingeben: ");
		zahl1 = tastatur.nextDouble();
		System.out.print("Zahl 2 eingeben: ");
		zahl2 = tastatur.nextDouble();
		System.out.print("Zahl 3 eingeben: ");
		zahl3 = tastatur.nextDouble();
		
		if(zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println("Zahl 3 ist gr��er als Zahl 1 oder 2!");
		}
		
	}
}