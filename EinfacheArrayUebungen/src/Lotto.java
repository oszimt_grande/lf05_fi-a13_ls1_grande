import java.util.Scanner;

public class Lotto {

	public static void main(String[] args) {
		int[] lottozahlen = {3, 7, 12, 18, 37, 42};
		
		// Zahlen ausgeben
		System.out.print("[ ");
		for (int i = 0; i < lottozahlen.length; i++) {
			System.out.print(" " + lottozahlen[i] + " ");
		}
		System.out.print(" ]\n\n");
		
		// Auf 12 bzw. 13 pr�fen
		boolean twelveCheck = false;
		boolean thirteenCheck = false;
		
		for (int i = 0; i < lottozahlen.length; i++) {
			if (lottozahlen[i] == 12) {
				twelveCheck = true;
			}
			
			if (lottozahlen[i] ==13) {
				thirteenCheck = true;
			}
		}
		
		if (twelveCheck)
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten");
		else 
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten");
		
		if (thirteenCheck)
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten");
		else
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten");
	}

}
