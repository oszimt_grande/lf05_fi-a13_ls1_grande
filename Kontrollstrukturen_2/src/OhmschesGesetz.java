import java.util.Scanner;


public class OhmschesGesetz {
	
	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		char groesse;
		double r;
		double u;
		double i;
		
		//Benutzereingabe
		System.out.print("Welche Gr��e wollen Sie berechnen? (R/U/I): ");
		groesse = tastatur.next().charAt(0);
		
		if (groesse == 'R' || groesse == 'r') {
			System.out.print("U in V eingeben: ");
			u = tastatur.nextDouble();
			System.out.print("I in A eingeben: ");
			i = tastatur.nextDouble();
			
			r = u/i;
			System.out.printf("Der Widerstand betr�gt %.2f Ohm.", r);
		}
		
		if (groesse == 'U' || groesse == 'u') {
			System.out.print("R in Ohm eingeben: ");
			r = tastatur.nextDouble();
			System.out.print("I in A eingeben: ");
			i = tastatur.nextDouble();
			
			u = r * i;
			System.out.printf("Die Spannung betr�gt %.2f V.", r);
		}
		
		if (groesse == 'I' || groesse == 'i') {
			System.out.print("R in Ohm eingeben: ");
			r = tastatur.nextDouble();
			System.out.print("U in V eingeben: ");
			u = tastatur.nextDouble();
			
			i = u / r;
			System.out.printf("Die Stromst�rke betr�gt %.2f A.", r);
		}
	}

}
