import java.util.Scanner;


public class BMI {

	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		char g;
		double gewicht;
		double groesse;
		double bmi;
		
		//Benutzereingabe:
		System.out.print("Geschlecht eingeben (w/m): ");
		g = tastatur.next().charAt(0);
		System.out.print("Gewicht in kg eingeben: ");
		gewicht = tastatur.nextDouble();
		System.out.print("Größe in cm eingeben: ");
		groesse = tastatur.nextDouble();
		
		//Berechnung
		bmi = gewicht / (groesse/100) * (groesse/100);
		
		if (g == 'w') {
			if (bmi < 19) {
				System.out.println("Sie sind untergewichtig!");
			}
			else if (bmi > 19 && bmi < 24) {
				System.out.println("Sie sind normalgewichtig!");
			}
			else if (bmi > 24) {
				System.out.println("Sie sind übergewichtig!");
			}
		}
		else if (g =='m') {
			if (bmi < 20) {
				System.out.println("Sie sind untergewichtig!");
			}
			else if (bmi > 20 && bmi < 25) {
				System.out.println("Sie sind normalgewichtig!");
			}
			else if (bmi > 25) {
				System.out.println("Sie sind übergewichtig!");
		}

	}

}
}
