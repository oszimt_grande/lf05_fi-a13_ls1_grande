import java.util.Scanner;

public class Million {

	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		int jahre = 0;
		double ein, zsatz;
		char wdh;
		
		do {
			//Benutzereingabe
			System.out.print("H�he der Einlage in � eingeben: ");
			ein = tastatur.nextDouble();
			System.out.print("Zinssatz in Prozent eingeben: ");
			zsatz = tastatur.nextDouble();
			
			zsatz = zsatz/100;
			
			while(ein < 1000000) {
				ein = ein + ein*zsatz;
				jahre = jahre + 1;
			}
			
			System.out.printf("Es dauert %d Jahre, bis Sie Million�r sind!\n", jahre);
			
			System.out.print("M�chten Sie eine weitere Berechnung durchf�hren? (j/n)");
			wdh = tastatur.next().charAt(0);
		}
		while (wdh == 'j' || wdh == 'J');
		
		System.out.println("Das Programm wird beendet.");
		
	}

}
