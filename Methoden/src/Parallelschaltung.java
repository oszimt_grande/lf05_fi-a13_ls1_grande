
public class Parallelschaltung {

	public static void main(String[] args) {
		//Deklaration
		double r1 = 12.7;
		double r2 = 25.5;
		
		System.out.println(parallelschaltung (r1, r2) + " Ohm");

	}
	
	public static double parallelschaltung (double r1, double r2) {
		return (r1 + r2) /2;
	}

}
