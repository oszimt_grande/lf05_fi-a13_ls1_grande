import java.util.Scanner;

public class Urlaub {

	static Scanner myScanner = new Scanner (System.in);
	
	public static void main(String[] args) {
		
		String Land;
		double gesBudget;
		double betragEUR;
		double betragFW;
		
		//Eingabe des Gesamtbudgets
		System.out.print("Gesamtbudget eingeben: ");
		gesBudget = myScanner.nextDouble();
		
		//Land eingeben
		System.out.print("Land eingeben (USA/JP/GB/CH/SW): ");
		Land = myScanner.next();
		
		System.out.print("Betrag in EUR eingeben: ");
		betragEUR = myScanner.nextDouble();
		
		switch(Land) {
			case "USA" : 
				betragFW = USA(betragEUR);
				System.out.printf("Betrag in US-Dollar: %.2f $\n", betragFW);
				break;
			case "JP" :
				betragFW = JP(betragEUR);
				System.out.printf("Betrag in Yen: %.2f �\n", betragFW);
				break;
			case "GB" :
				betragFW = GB(betragEUR);
				System.out.printf("Betrag in britischen Pfund: %.2f �\n", betragFW);
				break;
			case "CH" :
				betragFW = CH(betragEUR);
				System.out.printf("Betrag in Schweizer Franken: %.2f CHF\n", betragFW);
				break;
			case "SW" :
				betragFW = SW(betragEUR);
				System.out.printf("Betrag in Schwedischen Kronen: %.2f SEK\n", betragFW);
				break;
		}
		
		// Betrag von Gesamtbudget abziehen
		gesBudget = gesBudget - betragEUR;
	}
	
	public static double USA(double betragEUR) {
		final double USA = 1.22;
		
		return betragEUR * USA;
	}
	
	public static double JP(double betragEUR) {
		final double JP = 126.5;
		
		return betragEUR * JP;
	}
	
	public static double GB(double betragEUR) {
		final double GB = 0.89;
		
		return betragEUR * GB;
	}
	
	public static double CH(double betragEUR) {
		final double CH = 1.08;
		
		return betragEUR * CH;
	}
	
	public static double SW(double betragEUR) {
		final double SW = 10.1;
		
		return betragEUR * SW;
	}

}
