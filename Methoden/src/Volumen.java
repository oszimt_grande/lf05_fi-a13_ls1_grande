public class Volumen {

	public static void main(String[] args) {
		//Deklaration
		double a = 1;
		double b = 2;
		double c = 3;
		double h = 4;
		double r = 5;
		
		//Ausf�hrung und Ausgabe
		System.out.println("W�rfelvolumen: " + Wuerfel(a) + "m�");
		System.out.println("Quadervolumen: " + (Quader(a, b, c) + "m�"));
		System.out.println("Pyramidenvolumen: " + Pyramide(a, h) + "m�");
		System.out.println("Kugelvolumen: " + Kugel(r) + "m�");
	}
	
	//Methoden
	
	//Wuerfel
	public static double Wuerfel(double a) {
		return a*a*a;
	}
	
	//Quader
	public static double Quader(double a, double b, double c) {
		return a*b*c;
	}
	
	//Pyramide
	public static double Pyramide(double a, double h) {
		return a*a*h/3;
	}
	
	//Kugel
	public static double Kugel(double r) {
		return 4/3*r*r*r*Math.PI;
	}

}
