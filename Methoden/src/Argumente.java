public class Argumente {
	public static void main(String[] args) {
		ausgabe(1, "Mana");
		ausgabe(2, "Elise");
		ausgabe(3, "Johanna");
		ausgabe(4, "Felizitas");
		ausgabe(5, "Karla");
		System.out.println(vergleichen(1, 2));
		System.out.println(vergleichen(1, 5));
		System.out.println(vergleichen(3, 4));
	}
	public static void ausgabe(int zahl, String name) {
		System.out.println(zahl + ": " + name);
	}
	public static boolean vergleichen(int arg1, int arg2) {
		return (arg1 + 8) < (arg2 * 3);
	}
}

/*
Erwartete Ausgabe:
Namen werden mit Zahlen ausgegeben; danach wird jeweils zur ersten Zahl
8 addiert und zweite Zahl mit drei multipliziert und verglichen, ob erste kleiner als
zweite ist -> true/false wird ausgegeben

tatsächliche Ausgabe: Wie Erwartung, aber noch jeweils Doppelpunkt zwischen Name und Zahl


*/