
public class Multiplikation {

	public static double main(String[] args) {
		//Deklaration
		double x = 2.36;
		double y = 7.87;
		
		//Ausgabe und Ausführung der Methode
		System.out.println(Multiplikation(x, y));
		
		
		return 0;
	}
	
	//Methode
	public static double Multiplikation(double a, double b) {
				
		return a*b;
	}

}
