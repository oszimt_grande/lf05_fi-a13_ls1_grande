public class Besitzer {
	//Attribute
	private String vorname, nachname;
	private Konto konto1, konto2;
	
	//Konstruktor(en)
	public Besitzer() {}
	public Besitzer(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}
	public Besitzer(String vorname, String nachname, Konto konto1, Konto konto2) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.konto1 = konto1;
		this.konto2 = konto2;
	}
	
	//Verwaltungsmethoden
	//getter
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public void setKonto1(Konto k) {
		this.konto1 = k;
	}
	public void setKonto2(Konto k) {
		this.konto2 = k;
	}
	//setter
	public String getVorname() {
		return this.vorname;
	}
	public String getNachname() {
		return this.nachname;
	}
	public String getKonto1() {
		return this.konto1.getIBAN();
	}
	public String getKonto2() {
		return this.konto2.getIBAN();
	}
	
	//allgemeine Methoden
	public void gesamtUebersicht() {
		System.out.printf("Gesamtübersicht\n\nKonto 1: %s\n", getKonto1());
		System.out.printf("Konto 2: %s\n", getKonto2());
	}
	
	public void gesamtGeld() {
		double summe = konto1.getSaldo() + konto2.getSaldo();
		System.out.printf("Gesamtvermögen: %.2f", summe);
	}
}
