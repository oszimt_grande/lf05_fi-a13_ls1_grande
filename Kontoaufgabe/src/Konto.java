public class Konto {
	//Attribute
	private String IBAN = "DE07101000106667217542";
	private long knr;
	private double saldo;
	
	//Konstruktoren
	public Konto() {}
	public Konto(String IBAN) {
		this.IBAN = IBAN;
	}
	public Konto(String IBAN, long knr, double saldo) {
		this.IBAN = IBAN;
		this.knr = knr;
		this.saldo = saldo;
	}
	
	//Verwaltungsmethoden
	public void setIBAN(String IBAN) {
		this.IBAN = IBAN;
	}
	public void setKnr(long knr) {
		this.knr = knr;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public String getIBAN() {
		return this.IBAN;
	}
	public long getKnr() {
		return this.knr;
	}
	public double getSaldo() {
		return this.saldo;
	}
	
	//allgemeine Methoden
	public void geldAbheben(double umsatz) {
		this.saldo = this.saldo - umsatz;
	}
	public void geldEinzahlen(double umsatz) {
		this.saldo = this.saldo + umsatz;
	}
	public void geldUeberweisen(double umsatz, Konto zielKonto) {
		this.saldo = this.saldo - umsatz;
		zielKonto.setSaldo(zielKonto.getSaldo() + umsatz);
	}
}
