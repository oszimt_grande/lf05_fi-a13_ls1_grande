public class KontoTest {

	public static void main(String[] args) {
		//Objekterzeugung
		Konto k1 = new Konto("DE31937495724858946509");
		Konto k2 = new Konto("DE38938475016000028345");
		Besitzer b1 = new Besitzer("Max", "Mustermann");
		
		//Objektmanipulation
		k1.setKnr(66672175);
		
		b1.setKonto1(k1);
		b1.setKonto2(k2);
		
		//weitere Methodenaufrufe
		k1.geldEinzahlen(1000);
		k1.geldAbheben(500);
		k1.geldUeberweisen(200, k2);
		System.out.printf("Saldo k1: %.2f\n", k1.getSaldo());
		System.out.printf("Saldo k2: %.2f\n", k2.getSaldo());
		
		b1.gesamtUebersicht();
		b1.gesamtGeld();
	}

}
