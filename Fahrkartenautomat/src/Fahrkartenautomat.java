﻿import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args)
    {
		double zuZahlen;
    	double rückgabebetrag;
    	char nochmal;
    	
        do {
        	zuZahlen = fahrkartenbestellungErfassen();
            
            rückgabebetrag = fahrkartenBezahlen(zuZahlen);
            
            fahrkartenAusgeben();
            
            rueckgeldAusgeben(rückgabebetrag);
            
            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine gute Fahrt.\n");
            
            //Abfrage nach erneuter Bestellung
            System.out.print("Wollen Sie eine weitere Bestellung tätigen? (j/n)\n");
            nochmal = tastatur.next().charAt(0);
        }
        while (nochmal == 'j' || nochmal == 'J');
    	System.out.println("\nVielen Dank für Ihren Einkauf!\n");
     
    }

    public static double fahrkartenbestellungErfassen() {
    	String[] fahrkartenBezeichnung = {"NULL", "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tagekarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	double[] ticketPreis = {0.0, 2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9};
    	byte ticketAuswahl, anzahlTickets;
    	
    	//Vorteil: Daten werden übersichtlicher gespeichert und bei Berechnung ist keine switch/case-Auswahlstruktur notwendig
    	
    	System.out.print("Wählbare Fahrscheine:\n\n");
    	for (int i = 1; i < fahrkartenBezeichnung.length; i++) {
    		System.out.print(i + ": " + fahrkartenBezeichnung[i] + " " + ticketPreis[i] + "€\n");
    	}
    	
    	System.out.println("Wählen Sie Ihre gewünschte Fahrkarte aus:\n");
    	ticketAuswahl = tastatur.nextByte();
    	
    	if (ticketAuswahl < 1 || ticketAuswahl > fahrkartenBezeichnung.length) {
    		do {
    			System.out.print(" >>falsche Eingabe<<\nIhre Wahl: ");
        		ticketAuswahl = tastatur.nextByte();
    		}
    		while (ticketAuswahl < 1 && ticketAuswahl > fahrkartenBezeichnung.length);
    	}
    	
    	System.out.print("Anzahl der Tickets: ");
    	anzahlTickets = tastatur.nextByte();
    	
    	double summe = ticketPreis[ticketAuswahl] * anzahlTickets;
    	
    	return summe;
    	
    	/*
    	Die alte Implementierung hat jeweils mit konkreten Werten (Bezeichnungen und Preisen) gearbeitet,
    	weshalb bei einer Änderung der Tickets an mehreren Stellen des Codes Veränderungen nötig waren
    	(z.B. bei der Ausgabe der Ticketnamen). Ebenfalls erforderte dies eine switch/case-Anweisung
    	zur Berechnung mit dem gewählten Ticket.
    	Bei der neuen Implementierung sind Kontrollstrukturen von der Arraylänge und nicht von fixer Zahl abhängig,
    	sodass bei Größenänderung des Arrays keine weiteren Codeanpassungen erforderlich sind.
    	Auch wird die switch/case-Anweisung obsolet, da die Auswahl nun durch Zugriff auf die jeweilige
    	Arrayposition erfolgt, wenngleich dies das Leerbleiben der nullten Position in den Arrays
    	erforderlich macht, damit die Auswahlnummern bei dem Wert 1 beginnen.
    	*/
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;

        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;

    }
    
    public static void fahrkartenAusgeben() {
        
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	 
    	if(rueckgabebetrag > 0.0)
         {
      	   System.out.println("Der Rückgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

             while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
             {
          	  System.out.println("2 EURO");
  	          rueckgabebetrag -= 2.0;
             }
             while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
             {
          	  System.out.println("1 EURO");
  	          rueckgabebetrag -= 1.0;
             }
             while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
             {
          	  System.out.println("50 CENT");
  	          rueckgabebetrag -= 0.5;
             }
             while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
             {
          	  System.out.println("20 CENT");
   	          rueckgabebetrag -= 0.2;
             }
             while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
             {
          	  System.out.println("10 CENT");
  	          rueckgabebetrag -= 0.1;
             }
             while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
             {
          	  System.out.println("5 CENT");
   	          rueckgabebetrag -= 0.05;
             }
         }
      }
}

/*
5.
Für das Speichern der Ticketanzahl ist der Datentyp byte verwendet worden,
da die Ticketanzahl stets ganzzahlig ist und an einem Fahrkartenautomaten realistischer-
weise nicht mehr als 127 Tickets (der maximale Wert des Datentyps) gleichzeitig gekauft
werden.
Der Einzelpreis der Tickets hingegen kann nach der Multiplikation ein Dezimalbruch sein,
da in den Rechenvorgang eine Gleitkommazahl involviert ist.

6.
Die Werte der beiden Variablen werden miteinander multipliziert. Hierbei ist zu beachten,
dass eine Ganzzahl mit einer Gleitkommazahl verrechnet wird, weshalb das Ergebnis eben-
falls als Gleitkommazahl, z.B. double oder float, gespeichert werden muss.
*/